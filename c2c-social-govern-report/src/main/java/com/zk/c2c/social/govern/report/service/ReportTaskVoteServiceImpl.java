package com.zk.c2c.social.govern.service.impl;

import com.zk.c2c.social.govern.domain.ReportTaskVote;
import com.zk.c2c.social.govern.mapper.ReportTaskVoteMapper;
import com.zk.c2c.social.govern.service.ReportTaskVoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chs
 * @since 2021-02-02
 */
@Service
public class ReportTaskVoteServiceImpl extends ServiceImpl<ReportTaskVoteMapper, ReportTaskVote> implements ReportTaskVoteService {

}
