package com.zk.c2c.social.govern.mapper;

import com.zk.c2c.social.govern.domain.ReportTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chs
 * @since 2021-02-02
 */
public interface ReportTaskMapper extends BaseMapper<ReportTask> {

}
