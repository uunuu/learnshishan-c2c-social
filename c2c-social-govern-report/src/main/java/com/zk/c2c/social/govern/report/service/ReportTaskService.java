package com.zk.c2c.social.govern.service;

import com.zk.c2c.social.govern.domain.ReportTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chs
 * @since 2021-02-02
 */
public interface ReportTaskService extends IService<ReportTask> {

}
