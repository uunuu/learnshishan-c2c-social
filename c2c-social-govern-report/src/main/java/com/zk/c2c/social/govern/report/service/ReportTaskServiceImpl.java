package com.zk.c2c.social.govern.service.impl;

import com.zk.c2c.social.govern.domain.ReportTask;
import com.zk.c2c.social.govern.mapper.ReportTaskMapper;
import com.zk.c2c.social.govern.service.ReportTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chs
 * @since 2021-02-02
 */
@Service
public class ReportTaskServiceImpl extends ServiceImpl<ReportTaskMapper, ReportTask> implements ReportTaskService {

}
