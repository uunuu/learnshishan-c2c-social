package com.zk.c2c.social.govern.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chs
 * @since 2021-02-02
 */
@Controller
@RequestMapping("/reportTask")
public class ReportTaskController {

}

