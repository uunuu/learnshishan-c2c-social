package com.zk.c2c.social.govern.reviewer.service;

import com.zk.c2c.social.govern.reviewer.api.ReviewerService;
import org.apache.dubbo.config.annotation.Service;

/**
 * dubbo没有通过Controller暴露接口，而是通过service进行调用
 */
@Service(
        version = "1.0.0",
        interfaceClass = ReviewerService.class,
        cluster = "failfast",
        loadbalance = "roundrobin"
)
public class ReviewerServiceImpl implements ReviewerService {

}
