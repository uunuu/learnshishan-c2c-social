package com.zk.c2c.social.govern.reward;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(DruidDataSource.class)
public class Application {
    public static void main(String[] args) {

    }
}
